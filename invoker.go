package src

const (
	ENTITY_CLASS_EVENT  = 1
	ENTITY_CLASS_METRIC = 2
)

type Batch struct {
	Entities *[]interface{}
	Class    uint8
}

type IInvoker interface {
	Enqueue(batch Batch)
}
