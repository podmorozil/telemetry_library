package src

import (
	"context"
	"sync"
	"time"
)

type Queue struct {
	context     context.Context
	invoker     IInvoker
	class       uint8
	entities    []interface{}
	sizeCurrent uint
	sizeMax     uint
	mutex       sync.Mutex
	waitGroup   sync.WaitGroup
}

func CreateQueue(context context.Context, invoker IInvoker, class uint8, size uint) *Queue {
	if size == 0 {
		return nil
	}

	queue := Queue{
		context: context,
		invoker: invoker,
		class:   class,
	}
	queue.createBuffer(size)
	return &queue
}

func (queue *Queue) Start() {
	go queue.worker()
}

func (queue *Queue) Stop() {
	queue.waitGroup.Wait()
}

func (queue *Queue) EnqueueOne(entity interface{}) {
	queue.mutex.Lock()
	defer queue.mutex.Unlock()

	queue.enqueue(entity)
}

func (queue *Queue) EnqueueMany(entities *[]interface{}) {
	queue.mutex.Lock()
	defer queue.mutex.Unlock()

	for _, entity := range *entities {
		queue.enqueue(entity)
	}
}

func (queue *Queue) enqueue(entity interface{}) {
	queue.entities[queue.sizeCurrent] = entity
	queue.sizeCurrent += 1

	if queue.sizeCurrent != queue.sizeMax {
		return
	}

	queue.flushBuffer()
}

func (queue *Queue) createBuffer(size uint) {
	queue.entities = make([]interface{}, size)
	queue.sizeCurrent = 0
	queue.sizeMax = size
}

func (queue *Queue) flushBuffer() {
	if queue.sizeCurrent == 0 {
		return
	}

	slice := queue.entities[:queue.sizeCurrent]
	queue.invoker.Enqueue(Batch{
		Class:    queue.class,
		Entities: &slice,
	})
	queue.createBuffer(queue.sizeMax)
}

func (queue *Queue) worker() {
	queue.waitGroup.Add(1)
	defer queue.waitGroup.Done()

	// TODO: make ticker configurable
	ticker := time.NewTicker(1 * time.Second)
	for {
		select {
		case <-ticker.C:
			queue.mutex.Lock()
			queue.flushBuffer()
			queue.mutex.Unlock()
		case <-queue.context.Done():
			return
		}
	}
}
