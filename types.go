package src

type Location struct {
	Region         string `json:"region"`
	DataCenter     string `json:"data_center"`
	Unit           string `json:"unit,omitempty"`
	VirtualMachine string `json:"virtual_machine"`
	Pod            string `json:"pod,omitempty"`
}

type Event struct {
	Location    Location          `json:"location"`
	Timestamp   int64             `json:"timestamp"`
	Nanoseconds int               `json:"nanoseconds"`
	Class       string            `json:"class"`
	Severity    int8              `json:"severity"`
	Service     string            `json:"service"`
	Name        string            `json:"name"`
	Arguments   map[string]string `json:"arguments"`
}

type EventsRequest struct {
	Events []Event `json:"events"`
}

type Metric struct {
	Location    Location `json:"location"`
	Timestamp   int64    `json:"timestamp"`
	Nanoseconds int      `json:"nanoseconds"`
	Class       string   `json:"class"`
	Service     string   `json:"service"`
	Name        string   `json:"name"`
	Value       string   `json:"value"`
}

type MetricsRequest struct {
	Metrics []Metric `json:"metrics"`
}

const (
	CLASS_A = "1"
	CLASS_B = "2"
	CLASS_C = "3"
)

const (
	SEVERITY_DEBUG = 0
	SEVERITY_INFORMATION = 1
	SEVERITY_WARNING = 2
	SEVERITY_ERROR = 3
	SEVERITY_FATAL = 4
)
